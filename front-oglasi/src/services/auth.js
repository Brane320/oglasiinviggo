import OglasiAxios from "../apis/OglasiAxios"

export const login = async function(username, password){
    const data = {
        username: username,
        password: password
    }
    try{
        const ret = await OglasiAxios.post('korisnici/auth', data);
        window.localStorage.setItem('jwt', ret.data);
        window.localStorage.setItem('username', username);
        
        
        console.log('Uspesno logovanje');

        
    }catch(error){
        console.log(error);
    }
    
    window.location.reload()
}

export const logout = function(){
    window.localStorage.removeItem('jwt');
    window.localStorage.removeItem('username');
    window.location.reload();
}