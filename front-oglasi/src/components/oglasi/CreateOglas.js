import React from 'react';
import OglasiAxios from '../../apis/OglasiAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
class CreateOglas extends React.Component {

    constructor(props){
        super(props);

    let oglas = {
        naziv: "",
        opis : "",
        urlSlike: "",
        cena: 0.0,
        kategorija : null,
        korisnik : null,
        grad : "",
        datumPostavljanja : ""
    }

    this.state = {oglas: oglas, kategorije: [], korisnik : {}};
}

componentDidMount(){
    this.getKategorije();
    this.setujKorisnika();
}

getKategorije(){
    OglasiAxios.get('/kategorije')
        .then(res => {
             // handle success
             console.log(res);
             this.setState({kategorije: res.data});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa ocitavanjem kategorija!');
        });
}

getKorisnikByUserName(username){
    OglasiAxios.get('/korisnici/' + username)
    .then(res => {
        // handle success
        console.log(res.data);
        let korisnik = {
            korisnickoIme : res.data.korisnickoIme,
            datumRegistracije : res.data.datumRegistracije,
            brojTelefona : res.data.brojTelefona
        }
       
        this.setState({korisnik : korisnik});
        
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Korisnik nije dobijen')
     });
}

setujKorisnika(){
    this.getKorisnikByUserName(window.localStorage['username']);
}

kategorijaSelectionChanged(e){
    let kategorijaId = e.target.value;

    let kategorija = this.state.kategorije.find((kategorijaDTO) => kategorijaDTO.id == kategorijaId);

    console.log(kategorija); 
    let oglas = this.state.oglas;

    oglas.kategorija = kategorija;

    this.setState({oglas : oglas});
}

renderKategorijeOptions() {
    return this.state.kategorije.map(kategorija => {
        return (
            <option key={kategorija.id} value={kategorija.id}>
                {kategorija.naziv}
            </option>
        )
    });
}


valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let oglas = this.state.oglas;
    oglas[name] = value;

    this.setState({oglas: oglas});
  }

  create(e) {
    
    let oglas = this.state.oglas;
    let oglasDTO = {
        naziv: oglas.naziv,
        opis : oglas.opis,
        urlSlike: oglas.urlSlike,
        cena: oglas.cena,
        kategorija : oglas.kategorija,
        korisnik : this.state.korisnik,
        grad : oglas.grad
     }
    OglasiAxios.post('/oglasi', oglasDTO)
    .then(res => {
        console.log(res);

        alert('Uspesno dodat oglas!');
        this.props.history.push("/oglasi");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesno kreiranje oglasa!');
    });

}

render(){
    return (
        <div className="mt-5">
        <Row>
        <Col></Col>
        <Col xs="10" sm="8" md="8">
        <Form>
            <Form.Label htmlFor="zNaziv">Naziv</Form.Label>
            <Form.Control id="zNaziv" name="naziv" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label htmlFor="zOpis">Opis</Form.Label>
            <Form.Control id="zOpis" name="opis" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zURL">URL Slike</Form.Label>
            <Form.Control  id="zURL" name="urlSlike" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zCena">Cena</Form.Label>
            <Form.Control id="zCena" name="cena" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zGrad">Grad</Form.Label>
            <Form.Control id="zGrad" name="grad" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label htmlFor="zKategorija">Kategorija</Form.Label>
            <InputGroup>
            <Form.Control as="select" id="zKategorija" name="kategorija" onChange={(e)=>this.kategorijaSelectionChanged(e)}>
            <option>Izaberite kategoriju</option>
            {this.renderKategorijeOptions()}
            </Form.Control>
            </InputGroup>
           
            <Button style={{ marginTop: "25px" }}onClick={(e) => this.create(e) }>Kreiraj</Button>
        </Form>
        </Col>
        <Col></Col>
        </Row>
        </div>
    )
}
}

export default CreateOglas;