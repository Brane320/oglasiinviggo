import React from 'react';
import OglasiAxios from '../../apis/OglasiAxios';
import {Table,Button, Col} from "react-bootstrap";




class Oglas extends React.Component {

    constructor(props){
        super(props);

        let oglas = {
            id : 0,
            naziv: "",
            opis : "",
            urlSlike: "",
            cena: 0.0,
            kategorija : {},
            korisnik : {},
            grad : "",
            datumPostavljanja : ""
        }

    this.state = {oglas : oglas};
}

componentDidMount(){
    this.getOglasById(this.props.match.params.id);
}

getOglasById(oglasId) {
    OglasiAxios.get('/oglasi/' + oglasId)
    .then(res => {
        // handle success
        console.log(res.data);
        let oglas= {
            id : res.data.id,
            naziv: res.data.naziv,
            opis : res.data.opis,
            urlSlike: res.data.urlSlike,
            cena: res.data.cena,
            kategorija : res.data.kategorija,
            korisnik : res.data.korisnik,
            grad : res.data.grad,
            datumPostavljanja : res.data.datumPostavljanja
        }
        console.log('Uspešno dobavljen oglas')
        this.setState({oglas : oglas});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Oglas nije dobijen')
     });
}

goToEdit(oglasId){
    this.props.history.push('/oglas/edit/' + oglasId);
}

delete(id) {
    if(this.state.oglas.korisnik.korisnickoIme == window.localStorage['username']){
    OglasiAxios.delete('/oglasi/' + id)
    .then(res => {
        // handle success
        console.log(res);
        this.props.history.push('/oglasi');
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Problem sa brisanjem ');
     });
    } else {
        alert('Ne možete da obrišete tuđ oglas!');
    }
}

renderOglas() {
    
    return (
        <tr key={this.state.oglas.id}>
          <td>{this.state.oglas.naziv}</td>
          <td>{this.state.oglas.opis}</td>
          <td>{this.state.oglas.cena}</td>
          <td>{this.state.oglas.kategorija.naziv}</td>
          <td>{this.state.oglas.grad}</td>
          <td>{this.state.oglas.datumPostavljanja}</td>
        </tr>
    )
 
}

render() {
     
    return (
<div className="mt-5">
        <img className="rounded mx-auto d-block" height="300" width="700" alt="slikaOglasa" src={this.state.oglas.urlSlike}/>
        <Table striped style={{marginTop:5}}>
            <thead className="thead-dark">
                <tr>
                    <th>Naziv oglasa</th>
                    <th>Opis</th>
                    <th>Cena</th>
                    <th>Kategorija</th>
                    <th>Grad</th>
                    <th>Datum postavljanja</th>
                </tr>
            </thead>
            <tbody>
                {this.renderOglas()}
            </tbody>                  
        </Table>
        <br/>
        <div className="d-flex justify-content-center">
        <Col xs="7" sm="5" md="5">
            <h3>Podaci o korisniku koji je postavio oglas</h3>
        <Table striped style={{marginTop:5}}>
            <thead className="thead-dark">
                <tr>
                    <th>Korisnicko ime</th>
                    <th>Datum registracije</th>
                    <th>Broj telefona</th>
                 </tr>
            </thead>
            <tbody>
                <td>{this.state.oglas.korisnik.korisnickoIme}</td>
                <td>{this.state.oglas.korisnik.datumRegistracije}</td>
                <td>{this.state.oglas.korisnik.brojTelefona}</td>
            </tbody>                  
        </Table>
        </Col>
    </div>

    <div className="d-flex justify-content-center">
        <Button style={{margin:3}} hidden={this.state.oglas.korisnik.korisnickoIme != window.localStorage['username']}  variant="danger" onClick={()=>this.delete(this.state.oglas.id)}>Brisanje</Button>
        <Button style={{margin:3}} hidden={this.state.oglas.korisnik.korisnickoIme != window.localStorage['username']}  variant="warning" onClick={()=>this.goToEdit(this.state.oglas.id)}>Izmena</Button>
    </div>
</div>
);
}

}
export default Oglas;