import React from 'react';
import {Table, Button, Form,Col,InputGroup,} from 'react-bootstrap'
import OglasiAxios from '../../apis/OglasiAxios';

class Oglasi extends React.Component {

    constructor(props) {
        super(props);

        

        this.state = {checked : false,kategorije : [], oglasi: [], brojStranice : 0,totalPages : 0,  search: { kategorijaId: -1, cenaOd : null,cenaDo : null, naziv : null}}
    }
    componentDidMount(){
        this.getOglase(0);
        this.getKategorije();
    }

    getOglase(brojStranice) {
        let config = {
            params: {
              brojStranice : brojStranice
            }
          };

          if (this.state.search.naziv != null) {
            config.params.naziv = this.state.search.naziv;
          }
      
          if (this.state.search.kategorijaId != -1) {
            config.params.kategorijaId = this.state.search.kategorijaId;
          }

          if (this.state.search.cenaOd != null) {
            config.params.cenaOd = this.state.search.cenaOd;
          }

          if (this.state.search.cenaDo != null) {
            config.params.cenaDo = this.state.search.cenaDo;
          }
        OglasiAxios.get('/oglasi', config)
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({brojStranice : brojStranice, oglasi : res.data,  totalPages : res.headers["total-pages"]});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Greska pri ocitavanju oglasa');
            });
    }
    
   

    pretraga() {
        this.getOglase(0)
    }

    pretragaInputChange(e) {
        let control = e.target;

        let name = control.name;
        let value = control.value;
    
        let pretraga = this.state.search;
        pretraga[name] = value;
    
        this.setState({ search : pretraga });
      }

    renderOglase() {
        return this.state.oglasi.map((oglas) => {
            return (
               <tr key={oglas.id}>
                <td><img height="120" width="250" src={oglas.urlSlike} alt="slikaOglasa"></img></td>
                <td><a style={{textDecoration:"none"}} href={'/#/oglas/' + oglas.id}>{oglas.naziv}</a></td>
                <td>{oglas.cena}</td>
                <td>{oglas.grad}</td>
                <td>{oglas.kategorija.naziv}</td>
                <td>
                <Button hidden={oglas.korisnik.korisnickoIme != window.localStorage['username']} style={{margin:3}} onClick={()=>this.delete(oglas.id)} variant="danger">Brisanje</Button> <br/>
                <Button hidden={oglas.korisnik.korisnickoIme != window.localStorage['username']} style={{margin:3}} onClick={()=>this.goToEdit(oglas.id)} variant="warning">Izmena</Button>
                </td>
             </tr>
            )
         })
    }

    prikaziSamoMoje() {
        return this.state.oglasi.map((oglas) => {
            if(oglas.korisnik.korisnickoIme ==  window.localStorage['username']){
            return (
               <tr key={oglas.id}>
                <td><img height="120" width="250" src={oglas.urlSlike} alt="slikaOglasa"></img></td>
                <td><a style={{textDecoration:"none"}}href={'/#/oglas/' + oglas.id}>{oglas.naziv}</a></td>
                <td>{oglas.cena}</td>
                <td>{oglas.grad}</td>
                <td>{oglas.kategorija.naziv}</td>
                <td>
                <Button hidden={oglas.korisnik.korisnickoIme != window.localStorage['username']} style={{margin:3}} onClick={()=>this.delete(oglas.id)} variant="danger">Brisanje</Button> <br/>
                <Button hidden={oglas.korisnik.korisnickoIme != window.localStorage['username']} style={{margin:3}} onClick={()=>this.goToEdit(oglas.id)} variant="warning">Izmena</Button>
                </td>
             </tr>
            )
            }
         })
    }

    goToEdit(oglasId){
        this.props.history.push('/oglas/edit/' + oglasId);
    }

    
    delete(id) {
        OglasiAxios.delete('/oglasi/' + id)
        .then(res => {
            // handle success
            console.log(res);
            window.location.reload()
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa brisanjem ');
         });
    }

    

    kategorijaSelectionChanged(e){
        let kategorijaId = e.target.value;
    
        let kategorija = this.state.kategorije.find((kategorijaDTO) => kategorijaDTO.id == kategorijaId);
    
        console.log(kategorija); 
        let search = this.state.search;
    
        search.kategorijaId = kategorijaId;
    
        this.setState({search : search});
    }
    
    renderKategorijeOptions() {
        return this.state.kategorije.map(kategorija => {
            return (
                <option key={kategorija.id} value={kategorija.id}>
                    {kategorija.naziv}
                </option>
            )
        });
    }


    getKategorije(){
        OglasiAxios.get('/kategorije')
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({kategorije: res.data});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Problem sa ocitavanjem kategorija!');
            });
    }

    promeniChecked(){
        this.setState({checked : !this.state.checked});
    }

    prikaziCheckBox(){
        return (
            <div>
            <Form.Label className="form-check-label" htmlFor="im">Prikaži samo moje oglase</Form.Label>
            <Form.Control className="form-check-input" type="checkbox" id="check" onChange={()=>this.promeniChecked()}></Form.Control>
            </div>
        )
    }

    render() {
     
        return (

            <div>

            <div className="mt-5">
            <h1>Pretraga</h1>
        
            <Col></Col>
            <Col xs="8" sm="6" md="6">
            <Form>
                <Form.Label htmlFor="kat">Kategorija</Form.Label>
                <InputGroup>
                <Form.Control as="select" id="kat" name="kategorija" onChange={e => this.kategorijaSelectionChanged(e)}>
                    <option>Izaberite kategoriju</option>
                {this.renderKategorijeOptions()}
                </Form.Control>
                <br/>
               
                </InputGroup>
                <br/>

                <Form.Label htmlFor="naz">Naziv</Form.Label>
                <Form.Control placeHolder="Unesite naziv oglasa koji želite da pretražite" id="naz" name="naziv" onChange={(e)=>this.pretragaInputChange(e)}/> <br/>

                <Form.Label htmlFor="min">Minimalna cena</Form.Label>
                <Form.Control placeHolder="Unesite minimalnu cenu za pretragu" id="min" name="cenaOd" onChange={(e)=>this.pretragaInputChange(e)}/> <br/>

                <Form.Label htmlFor="im">Maksimalna cena</Form.Label>
                <Form.Control placeHolder="Unesite maksimalnu cenu za pretragu" id="max" name="cenaDo" onChange={(e)=>this.pretragaInputChange(e)}/> 
   
                <Button style={{ marginTop: "25px" }}onClick={(e) => this.pretraga()}>Pretrazi</Button>
                <br/>
                <br/> 
                {window.localStorage['jwt'] ? this.prikaziCheckBox() : ""}
            </Form>
            </Col>
            </div>
        
            <div  className="d-flex justify-content-center">
                <Col xs="10" sm="8" md="8">
            <Table striped style={{marginTop:10}}>
                <thead className="thead-dark">
                    <tr>
                        <th>Slika</th>
                        <th>Ime</th>
                        <th>Cena</th>
                        <th>Grad</th>
                        <th>Kategorija</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.checked ? this.prikaziSamoMoje() : this.renderOglase()}
                </tbody>                  
            </Table>
            </Col>
            </div>
            <div style={{marginLeft : "778px"}}>
                    <Button style={{margin:3}} disabled={this.state.brojStranice==0} onClick={()=>this.getOglase(this.state.brojStranice -1)}>Prethodna stranica</Button>
                    <Button disabled={this.state.brojStranice==this.state.totalPages-1} onClick={()=>this.getOglase(this.state.brojStranice+1)}>Sledeća stranica</Button>
                </div>
            </div>
    );
    }

}

export default Oglasi;