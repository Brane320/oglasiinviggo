import React from 'react';
import OglasiAxios from '../../apis/OglasiAxios';
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";

class EditOglas  extends React.Component {
    constructor(props){
        super(props);

        let oglas = {
            id : -1,
            naziv: "",
            opis : "",
            urlSlike: "",
            cena: 0.0,
            kategorija : {},
            grad : "",
            datumPostavljanja : ""
        }

    

    this.state = {oglas: oglas, kategorije : []};

}

componentDidMount(){
    this.getKategorije();
    this.getOglasById(this.props.match.params.id);
}

getOglasById(oglasId) {
    OglasiAxios.get('/oglasi/' + oglasId)
    .then(res => {
        // handle success
        console.log(res.data);
        let oglas= {
            id : res.data.id,
            naziv: res.data.naziv,
            opis : res.data.opis,
            urlSlike: res.data.urlSlike,
            cena: res.data.cena,
            korisnik : res.data.korisnik,
            kategorija : res.data.kategorija,
            grad : res.data.grad,
        }
        console.log('Uspešno dobavljen oglas')
        this.setState({oglas : oglas});
        
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Oglas nije dobijen')
     });
}


getKategorije(){
    OglasiAxios.get('/kategorije')
        .then(res => {
             // handle success
             console.log(res);
             this.setState({kategorije: res.data});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Problem sa ocitavanjem kategorija!');
        });
}

kategorijaSelectionChanged(e){
    let kategorijaId = e.target.value;

    let kategorija = this.state.kategorije.find((kategorijaDTO) => kategorijaDTO.id == kategorijaId);

    console.log(kategorija); 
    let oglas = this.state.oglas;

    oglas.kategorija = kategorija;

    this.setState({oglas : oglas});
}

renderKategorijeOptions() {
    return this.state.kategorije.map(kategorija => {
        return (
            <option key={kategorija.id} value={kategorija.id}>
                {kategorija.naziv}
            </option>
        )
    });
}

valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let oglas = this.state.oglas;
    oglas[name] = value;

    this.setState({oglas: oglas});
  }

  edit(){
    let oglas = this.state.oglas;
    let oglasDTO = {
        id : oglas.id,
        naziv: oglas.naziv,
        opis : oglas.opis,
        urlSlike: oglas.urlSlike,
        cena: oglas.cena,
        kategorija : oglas.kategorija,
        korisnik : oglas.korisnik,
        grad : oglas.grad,
        datumPostavljanja : oglas.datumPostavljanja
    }
    OglasiAxios.put('/oglasi/' + this.state.oglas.id, oglasDTO)
    .then(res => {
        console.log(res);
         alert('Oglas uspesno izmenjen');
        this.props.history.push("/oglasi");
    })
    .catch(error => {
        console.log(error);
        alert('Error! Neuspesno editovanje oglasa!');
    });
}

render(){
    return (
        <div className="mt-5">
        <Row>
        <Col></Col>
        <Col xs="10" sm="8" md="8">
        <Form>
            <Form.Label htmlFor="zNaziv">Naziv</Form.Label>
            <Form.Control id="zNaziv" name="naziv"  value={this.state.oglas.naziv} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label htmlFor="zOpis">Opis</Form.Label>
            <Form.Control id="zOpis" name="opis" value={this.state.oglas.opis} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zURL">URL Slike</Form.Label>
            <Form.Control  id="zURL" name="urlSlike" value={this.state.oglas.urlSlike} onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zCena">Cena</Form.Label>
            <Form.Control id="zCena" name="cena" value={this.state.oglas.cena}  onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zGrad">Grad</Form.Label>
            <Form.Control id="zGrad"  value={this.state.oglas.grad} name="grad" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label htmlFor="zKategorija">Kategorija</Form.Label>
            <InputGroup>
            <Form.Control as="select" id="zKategorija" name="kategorija" value={this.state.oglas.kategorija.id} onChange={(e)=>this.kategorijaSelectionChanged(e)}>
            <option>Izaberi kategoriju</option>
            {this.renderKategorijeOptions()}
            </Form.Control>
            </InputGroup>
           
            <Button style={{ marginTop: "25px" }}onClick={() => this.edit() }>Izmeni oglas</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>
    </div>
    )
}

}

export default EditOglas;