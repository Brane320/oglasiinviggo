import React from 'react';
import OglasiAxios from '../../apis/OglasiAxios';
import { Form, Button, Row, Col} from "react-bootstrap";

class Register extends React.Component {

    constructor(props){
        super(props);
        

    let korisnikRegistracijaDto = {
        korisnickoIme: "",
        
        sifra : "",
        brojTelefona : ""
    }

    this.state = {korisnikRegistracijaDto : korisnikRegistracijaDto};
}



valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    console.log(name + ", " + value);

    let korisnikRegistracijaDto = this.state.korisnikRegistracijaDto;
    korisnikRegistracijaDto[name] = value;

    this.setState({ korisnikRegistracijaDto: korisnikRegistracijaDto });
  }

  register(e){
        
    let korisnikReg = this.state.korisnikRegistracijaDto;
    let korisnikRegistracijaDto = {
        korisnickoIme: korisnikReg.korisnickoIme,
        sifra : korisnikReg.sifra,
        brojTelefona : korisnikReg.brojTelefona
        
        


    }
    
      

   
    OglasiAxios.post('/korisnici' , korisnikRegistracijaDto)
    .then(res => {
        console.log(res);

        alert('Uspesna registracija');
        this.props.history.push("/login");
    })

    .catch(error => {
        console.log(error);

        alert('Error! Neuspesna registracija');
    });
 
}


render() {
    return (
        <div className="mt-5">
        <Row className="justify-content-center">
        <Col></Col>
        <Col xs="8" sm="6" md="6">
        <Form>
            <Form.Label htmlFor="zkorisnicko">Korisničko ime</Form.Label>
            <Form.Control id="zkorisnicko" name="korisnickoIme" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
            
            <Form.Label id="zlozinka">Šifra</Form.Label>
            <Form.Control type="password" id="zlozinka" name="sifra" onChange={(e)=>this.valueInputChanged(e)}/> <br/>
           
            <Form.Label  id="zBrojTel">Broj telefona</Form.Label>
            <Form.Control  id="zBrojTel" name="brojTelefona" onChange={(e)=>this.valueInputChanged(e)}/>

            <Button style={{ marginTop: "25px" }} onClick={(e) => this.register(e)}>Registruj se</Button>
        </Form>
        </Col>

        
        <Col></Col>
        </Row>
    </div>

    

    );

   }

}












export default Register;