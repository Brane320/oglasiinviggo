import React from "react";
import {Form, Button, Row, Col} from 'react-bootstrap'
import {login} from "../../services/auth"

class Login extends React.Component {
  constructor() {
    super();

    this.state = { username: "", password: "" };
  }

  onInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let change = {};
    change[name] = value;
    this.setState(change);
  }

  render() {
    return (
      <div className="mt-5">
      <Row className="justify-content-center">
        <Col md={6}>        
          <Form>
            
              <Form.Label>Korisničko ime</Form.Label>
              <Form.Control type="text" name="username" onChange = {(e) => this.onInputChange(e)}/> <br/>
            
            
              <Form.Label>Šifra</Form.Label>
              <Form.Control type="password" name="password" onChange = {(e) => this.onInputChange(e)}/>
            
            <Button style={{ marginTop: "25px" }} onClick={() => {login(this.state.username, this.state.password)}}>Uloguj se</Button>
          </Form>
        </Col>
      </Row>
      </div>
    );
  }
}

export default Login;