import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Link, Switch, Route } from 'react-router-dom';
import { Navbar, Nav, Container,Button} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/login/Login"
import Register from "./components/register/Register"
import {logout} from './services/auth'
import Oglasi from './components/oglasi/Oglasi';
import EditOglas from './components/oglasi/EditOglas';
import Oglas from './components/oglasi/Oglas';
import CreateOglas from './components/oglasi/CreateOglas';
import './index.css';






class App extends React.Component {
  

  render() {
    return (
        
      <div>
      <Router>
        <Navbar className="nav justify-content-center fixed-top" expand bg="dark" variant="dark">
          <Navbar.Brand as={Link} to="/oglasi">
              Homepage
          </Navbar.Brand>
         
          
          <Nav>

          {window.localStorage['jwt'] ? 
                       <Nav.Link as={Link} to="/createoglas">Dodaj novi oglas</Nav.Link> : ""
          } 
            

          
            {window.localStorage['jwt'] ? 
                      "" :
                      <Nav.Link as={Link} to="/register">Sign up</Nav.Link>
            }

           
            {window.localStorage['jwt'] ? 
                      <Button onClick = {()=>logout()}>Sign out</Button> :
                      <Nav.Link as={Link} to="/login">Login</Nav.Link>
            }

            {window.localStorage['jwt'] ? 
              <Nav.Link disabled as={Link}> Ulogovani korisnik: {window.localStorage['username']}</Nav.Link> 
              : ""
            }
            
          </Nav>

          
        
         
  
        
            </Navbar>
          <Container style={{paddingTop:"25px"}}>
            <Switch>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/oglasi" component={Oglasi}/>
            <Route exact path="/oglas/edit/:id" component={EditOglas}/>
            <Route exact path="/oglas/:id" component={Oglas}/>
            <Route exact path="/createoglas" component={CreateOglas}/>
            </Switch>
          </Container>
        </Router>
        
      </div>
    )
}
};

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);