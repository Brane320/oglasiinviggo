

              
INSERT INTO kategorija (naziv) VALUES ('Clothing');
INSERT INTO kategorija (naziv) VALUES ('Tools');
INSERT INTO kategorija (naziv) VALUES ('Sports');
INSERT INTO kategorija (naziv) VALUES ('Accessories');
INSERT INTO kategorija (naziv) VALUES ('Furniture');
INSERT INTO kategorija (naziv) VALUES ('Pets');
INSERT INTO kategorija (naziv) VALUES ('Games');
INSERT INTO kategorija (naziv) VALUES ('Books');
INSERT INTO kategorija (naziv) VALUES ('Technology');

INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('miroslav',064444223,'2020-11-15','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6');
              
INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('tamara',064464223,'2021-01-15','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky');

INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('petar',064465227,'2021-02-15','$2a$10$oAJCSA6rmIRF5TDSkHLPzeX.b70Q4iQnkpWxtjQKtfUKinNrCIoX2');

INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('maja',066465629,'2021-05-19','$2a$10$ZC0R84sow9UXtbocg5tJoOoJkdMKdkGmIV8/3WxU7gNopKiXzDH.2');
              
INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('nikola',061040499,'2020-05-19','$2a$10$Zg3Thf9GgwCAoYoZ/RIs2unIvE/xQM2XSXP5RVFy88ZA0LWHtoAfG');
              
INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('jelena', 63228882, '2021-07-23', '$2a$10$co8E8l3kekSMv5q559rjVe8a.9bIimsC0N1fM7mCLiqN6GtJlv13W');

INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('elena', 60300250, '2021-07-23', '$2a$10$pAWAO5k37JvLB6sSYe41seP/VUDzr/prNJns7r5qMl0h/1gTtm63i');

INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('mia', 64509555, '2021-07-23', '$2a$10$MkQ2EUtPq8owXiQ3ChMd/OgA3cAmGoTJrHgIQ5j5SPJCu8rm4rzU6');

INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('stefan', 61293332, '2021-07-23', '$2a$10$0W2Lek.LGY4XOABa53f5zO5vXgqWpaGRtqRPRUvvdeYvxt0KrXhSa');
              
INSERT INTO korisnik (korisnicko_ime,broj_telefona,datum_registracije,sifra)
              VALUES ('ana', 65998510, '2021-07-23', '$2a$10$fCv.nuiR4SU4Q.GFSti0xuqlxSi3h8TsCd9NzUYKTsSg/fCJ.AvZS');

            

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(1,10200,'2021-02-17','Novi Sad','Audi A4','U odlicnom stanju','https://img.halooglasi.com//slike/oglasi/Thumbs/201101/m/audi-a4-2-0-tdi-s-line-5425636023304-71793295169.jpg',4,'miroslav');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(2,65000,'2020-08-20','Beograd','Audi A8','Kupljen nov u Srbiji','https://gcdn.polovniautomobili.com/user-images/thumbs/1760/17605535/ac8422a510c7-800x600.jpg',3,'miroslav');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(3,92500,'2020-11-27','Novi Sad','BMW 840d','Za sve informacije pozvati','https://gcdn.polovniautomobili.com/user-images/thumbs/1769/17698328/e5902b4b3a21-800x600.jpg',3,'tamara');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(4, 600000, '2021-07-23', 'Beograd', 'Penthouse', 'U centru BG nova gradnja.', 'https://cf.bstatic.com/xdata/images/hotel/max1024x768/238594415.jpg?k=0824ef8e59cc54410310d1c07051f3fbd4be04aba3553009eeff05e741051e28&o=&hp=1', 5, 'ana');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(5, 950000, '2021-07-23', 'Budva', 'Kuća', 'Na 1.5km od mora.', 'https://www.lobazzini.com/uploads/sliderimage/exceptional_luxury_mansion_marbella.jpg', 5, 'ana');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(6, 6500, '2021-05-23', 'Beograd', 'Omega Seamaster', 'Nov sat neotpakovan.', 'https://da8t9y7300ntx.cloudfront.net/wp-content/uploads/sites/6/2020/04/omega-seamaster-300.jpg',4, 'stefan');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(7, 122000, '2021-07-23', 'Novi Sad', 'Mercedes Benz S400d', 'Pod garancijom.', 'https://www.automagazin.ba/wp-content/uploads/2021/01/20-s-class-2021-mercedes.jpg', 3, 'nikola');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(8, 412000, '2020-07-23', 'Kotor', 'Monterey 385SE', 'Star 2 godine odlicno stanje.', 'https://advancelocal-adapter-image-uploads.s3.amazonaws.com/expo.advance.net/img/9c2760155e/width2048/cdd_dsc0333.jpeg', 3, 'nikola');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(9, 290, '2019-08-25', 'Novi Sad', 'Seiko 5 Sports', 'Elegantni model iz kolekcije 5 Sports ima plavi brojčanik i čeličnu narukvicu u srebrnoj boji.Kalibar mehanizma 4R36 koji kuca u kućištu sata ima snagu rezerve do 41h. Lako se kombinuje uz svakodnevni stil.', 'https://www.satoviberic.rs/images/product/medium2/Seiko-5-Sports-Sports-Style-SRPD53K1-280-%E2%82%AC-2-Horas-y-Minutos.jpg', 4, 'nikola');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(10, 110, '2021-04-25', 'Beograd', 'Carolina Herrera Bad Boy', 'Bad Boy je orijentalno aromatični parfem u kome se suprotstavljaju svežina žalfije i zelenog bergamota sa mešavinom paprike i mračnim tonovima biljke tonka, kakaoa i ćilibara.', 'https://www.luxlife.rs/storage/posts/seo/2021/Feb/230044/miris-savrsenog-muskarca-carolina-herrera-bad-boy.jpeg', 4, 'stefan');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(11, 3000, '2021-06-25', 'Novi sad', 'Lenovo Legion 7', 'Tip grafičke karte:GeForce RTX 2070,Max-Q RAM memorija:32GB,HDD SSD:1TB SSD', 'https://img.gigatron.rs/img/products/large/LENOVO-Legion-7-15IMH05-81YT002LYA-25.png', 9, 'stefan');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(12, 350, '2021-07-25', 'Beograd', 'Samsung Galaxy S10+', 'Star godinu dana bez tragova koriscenja.', 'https://cdn.mos.cms.futurecdn.net/3KzX4FcFRu3nvFeyQp5AgP.jpg',9, 'tamara');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(13, 990, '2021-07-25', 'Novi Sad', 'Samsung Galaxy S21 Ultra', 'Originalno fabričko pakovanje Samsung Galaxy S21 Ultra 5G mobilnog telefona sa garancijom. ', 'https://i1.wp.com/pcpress.rs/wp-content/uploads/2021/02/Galaxy-S21-Ultra_2-1.jpg?fit=800%2C450&ssl=1', 9, 'tamara');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(14, 999, '2021-01-25', 'Novi Sad', 'Samsung Galaxy Note 20 Ultra', 'Originalno fabričko pakovanje Samsung Galaxy Note 20 Ultra 5G mobilnog telefona sa garancijom. ', 'https://www.benchmark.rs/assets/img/news/big_thumb/cdf261ad596b8cc528e1e6d9070daddd.jpg', 9, 'tamara');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(15, 850, '2020-07-02', 'Subotica', 'Iphone 12', 'Originalno fabričko pakovanje Apple iPhone 12 256GB mobilnog telefona sa garancijom.', 'https://www.counterpointresearch.com/wp-content/uploads/2021/06/Featured-Image-1.jpg', 9, 'tamara');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(16, 150, '2020-07-25', 'Novi Sad', 'Americki Stafordski Terijer', 'Stene ostenjeno pre 3 meseca.', 'https://www.thesprucepets.com/thmb/-T33aoV17NtniJDkslLBJQJPg2c=/2121x1414/filters:no_upscale():max_bytes(150000):strip_icc()/AmericanStaffordshireterrierpuppy-GettyImages-159396531-59b857ff60ea41e6af62163039f68cf3.jpg', 6, 'mia');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(17, 400, '2021-05-10', 'Beograd', 'Dobermann', 'Stene ostenjeno pre 2 meseca.', 'https://gfp-2a3tnpzj.stackpathdns.com/wp-content/uploads/2019/11/Howard-m2.jpg', 6, 'mia');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(18, 139000, '2021-07-25', 'Novi Sad', 'Audi SQ8', '2X S Line. Bang & Olufsen ozvucenje. ', 'https://autoblog.rs/gallery/108/197531-abt%20q8%201.jpg', 3, 'elena');

INSERT INTO oglas (id,cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(19, 200000, '2021-07-25', 'Monte Carlo', 'Bentley Continental GT', '2021 godiste uvoz iz Nemacke W12 motor.', 'http://st.automobilemag.com/uploads/sites/5/2021/05/2022-Bentley-Continental-GT-Speed-7.jpg', 3, 'elena');

INSERT INTO oglas (cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
(99999, '2021-07-25', 'Novi Sad', 'Maserati Quattroporte', '2020. 3.8 V8 581 HP', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-maserati-quattroporte-s-q4-trahan-015-1587075544.jpg?crop=0.593xw:0.445xh;0.351xw,0.305xh&resize=1200:*', 3, 'elena');

INSERT INTO oglas (cena,datum_postavljanja,grad,naziv,opis,url_slike,kategorija_id,korisnik_korisnicko_ime) VALUES 
( 25000, '2021-01-15', 'Beograd', 'Volkswagen Touareg', '2016 godina. Registrovan i osiguran.', 'https://www.automanijak.com/resources/images/variant/531/touareg_4.jpg', 3, 'elena');
