package oglasi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import oglasi.model.Kategorija;

@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija,Long> {
	
}
