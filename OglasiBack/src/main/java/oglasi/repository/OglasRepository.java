package oglasi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import oglasi.model.Oglas;

@Repository
public interface OglasRepository extends JpaRepository<Oglas,Long>{
	
	
	Page<Oglas> findAllByOrderByDatumPostavljanjaDesc(Pageable pageable);
	
	Page<Oglas> findByKategorijaIdAndNazivIgnoreCaseContainsAndCenaBetween(Long kategorijaId,String naziv,Double cenaOd,Double cenaDo,Pageable pageable);

	Page<Oglas> findByNazivIgnoreCaseContainsAndCenaBetween(String naziv,Double cenaOd,Double cenaDo,Pageable pageable);
}
