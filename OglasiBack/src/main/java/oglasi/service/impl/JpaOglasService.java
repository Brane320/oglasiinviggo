package oglasi.service.impl;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import oglasi.model.Oglas;
import oglasi.repository.OglasRepository;
import oglasi.service.OglasService;

@Service
public class JpaOglasService implements OglasService{
	
	@Autowired
	private OglasRepository oglasRepository;

	@Override
	public Page<Oglas> getAll(int brojStranice) {
		return oglasRepository.findAllByOrderByDatumPostavljanjaDesc(PageRequest.of(brojStranice, 20));
	
	}

	@Override
	public Oglas getOne(Long id) {
		return oglasRepository.getOne(id);
	}

	@Override
	public Oglas save(Oglas oglas) {
		oglas.setDatumPostavljanja(LocalDate.now());
		
		return oglasRepository.save(oglas);
	}

	@Override
	public Oglas update(Oglas oglas) {
		return oglasRepository.save(oglas);
	}

	@Override
	public Optional<Oglas> findById(Long id) {
		return oglasRepository.findById(id);
	}

	

	@Override
	public Page<Oglas> find(Long kategorijaId, String naziv, Double cenaOd, Double cenaDo, int brojStranice) {
		if(naziv == null) {
			naziv = "";
		}
		
		if(cenaOd == null) {
			cenaOd = 0.0;
		}
		
		if(cenaDo == null) {
			cenaDo = Double.MAX_VALUE;
		}
		
		if(kategorijaId == null) {
			return oglasRepository.findByNazivIgnoreCaseContainsAndCenaBetween(naziv, cenaOd, cenaDo,PageRequest.of(brojStranice, 20));
		}
		return oglasRepository.findByKategorijaIdAndNazivIgnoreCaseContainsAndCenaBetween(kategorijaId, naziv, cenaOd, cenaDo, PageRequest.of(brojStranice, 20));
		
	}

	@Override
	public Oglas delete(Long id) {
		Optional<Oglas> oglas = oglasRepository.findById(id);
		
		if(oglas.isPresent()) {
			oglasRepository.deleteById(id);
			return oglas.get();
		} else {
			return null;
		}
	}
	
	

}
