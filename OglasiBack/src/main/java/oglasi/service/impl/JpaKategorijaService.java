package oglasi.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import oglasi.model.Kategorija;
import oglasi.repository.KategorijaRepository;
import oglasi.service.KategorijaService;

@Service
public class JpaKategorijaService implements KategorijaService {
	
	@Autowired
	private KategorijaRepository kategorijaRepository;

	@Override
	public List<Kategorija> findAll() {
		return kategorijaRepository.findAll();
	}

	@Override
	public Kategorija getOne(Long id) {
		return kategorijaRepository.getOne(id);
	}

	@Override
	public Optional<Kategorija> findById(Long id) {
		return kategorijaRepository.findById(id);
	}

}
