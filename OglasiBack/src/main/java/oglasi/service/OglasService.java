package oglasi.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import oglasi.model.Oglas;

public interface OglasService {
	
	Page<Oglas> getAll(int brojStranice);
	
	Oglas getOne(Long id);
	
	Oglas save(Oglas oglas);
	
	Oglas update(Oglas oglas);
	
	Optional<Oglas> findById(Long id);
	
	Page<Oglas> find(Long kategorijaId,String ime,Double cenaOd,Double cenaDo,int brojStranice);
	
	Oglas delete(Long id);

}
