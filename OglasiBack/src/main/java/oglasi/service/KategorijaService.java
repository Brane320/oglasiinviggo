package oglasi.service;

import java.util.List;
import java.util.Optional;

import oglasi.model.Kategorija;

public interface KategorijaService {

	List<Kategorija> findAll();
	
	Kategorija getOne(Long id);
	
	Optional<Kategorija> findById(Long id);
}
