package oglasi.web.dto;

import oglasi.model.Kategorija;
import oglasi.model.Korisnik;

public class OglasDTO {

	
	private Long id;
	
	
	private String naziv;
	
	
	private String opis;
	
	
	private String urlSlike;
	
	
	private Double cena;
	
	
	private Kategorija kategorija;
	
	
	private Korisnik korisnik;
	
	
	private String grad;
	
	
	private String datumPostavljanja;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getOpis() {
		return opis;
	}


	public void setOpis(String opis) {
		this.opis = opis;
	}


	public String getUrlSlike() {
		return urlSlike;
	}


	public void setUrlSlike(String urlSlike) {
		this.urlSlike = urlSlike;
	}


	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}


	public Kategorija getKategorija() {
		return kategorija;
	}


	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}


	public Korisnik getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}


	public String getGrad() {
		return grad;
	}


	public void setGrad(String grad) {
		this.grad = grad;
	}


	public String getDatumPostavljanja() {
		return datumPostavljanja;
	}


	public void setDatumPostavljanja(String datumPostavljanja) {
		this.datumPostavljanja = datumPostavljanja;
	}
	
	
}
