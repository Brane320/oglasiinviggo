package oglasi.web.dto;

public class KorisnikDTO {

	
	private String korisnickoIme;
	
	private String datumRegistracije;
	
	private Integer brojTelefona;

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getDatumRegistracije() {
		return datumRegistracije;
	}

	public void setDatumRegistracije(String datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}

	public Integer getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(Integer brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	
	
	

}
