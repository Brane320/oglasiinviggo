package oglasi.web.dto;

import javax.validation.constraints.NotNull;

public class KorisnikRegistracijaDTO extends KorisnikDTO {
	
	@NotNull
	private String korisnickoIme;
	
	@NotNull
	private String sifra;
	
	private String datumRegistracije;
	
	@NotNull
	private Integer brojTelefona;

	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	
	

	public String getDatumRegistracije() {
		return datumRegistracije;
	}



	public void setDatumRegistracije(String datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}



	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public Integer getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(Integer brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	
	

}
