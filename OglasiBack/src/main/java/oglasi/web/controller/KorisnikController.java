package oglasi.web.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import oglasi.model.Korisnik;
import oglasi.security.TokenUtils;
import oglasi.service.KorisnikService;
import oglasi.support.KorisnikDTOToKorisnik;
import oglasi.support.KorisnikToKorisnikDTO;
import oglasi.web.dto.AuthKorisnikDto;
import oglasi.web.dto.KorisnikDTO;
import oglasi.web.dto.KorisnikRegistracijaDTO;



@RestController
@RequestMapping(value = "/api/korisnici", produces = MediaType.APPLICATION_JSON_VALUE)
public class KorisnikController {
	
	
	    @Autowired
	    private KorisnikService korisnikService;

	    @Autowired
	    private KorisnikDTOToKorisnik toKorisnik;

	    @Autowired
	    private KorisnikToKorisnikDTO toKorisnikDto;

	    @Autowired
	    private AuthenticationManager authenticationManager;

	    @Autowired
	    private UserDetailsService userDetailsService;

	    @Autowired
	    private TokenUtils tokenUtils;

	    @Autowired
	    private PasswordEncoder passwordEncoder;
	    
	    
	    @PostMapping
	    public ResponseEntity<KorisnikDTO> create(@RequestBody @Validated KorisnikRegistracijaDTO dto){
	    	
	    	dto.setDatumRegistracije(LocalDate.now().toString());

	      
	        Korisnik korisnik = toKorisnik.convert(dto);
	       
	     
	        String encodedPassword = passwordEncoder.encode(dto.getSifra());
	       
	        korisnik.setSifra(encodedPassword);
	        

	        return new ResponseEntity<>(toKorisnikDto.convert(korisnikService.save(korisnik)), HttpStatus.CREATED);
	    }

	   

	    @GetMapping("/{korisnickoIme}")
	    public ResponseEntity<KorisnikDTO> get(@PathVariable String korisnickoIme){
	    	Korisnik korisnik = korisnikService.findByKorisnickoIme(korisnickoIme);
	    	
	    	return new ResponseEntity<>(toKorisnikDto.convert(korisnik),HttpStatus.OK);
	    }

	    
	    @GetMapping
	    public ResponseEntity<List<KorisnikDTO>> get(@RequestParam(defaultValue="0") int page){
	        Page<Korisnik> korisnici = korisnikService.findAll(page);
	        return new ResponseEntity<>(toKorisnikDto.convert(korisnici.getContent()), HttpStatus.OK);
	    }

	  

	    
	    @RequestMapping(path = "/auth", method = RequestMethod.POST)
	    public ResponseEntity authenticateUser(@RequestBody AuthKorisnikDto dto) {
	        // Perform the authentication
	        UsernamePasswordAuthenticationToken authenticationToken =
	                new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
	        Authentication authentication = authenticationManager.authenticate(authenticationToken);
	        SecurityContextHolder.getContext().setAuthentication(authentication);
	        try {
	            // Reload user details so we can generate token
	            UserDetails userDetails = userDetailsService.loadUserByUsername(dto.getUsername());
	            return ResponseEntity.ok(tokenUtils.generateToken(userDetails));
	            
	        } catch (UsernameNotFoundException e) {
	            return ResponseEntity.notFound().build();
	        }
	    }
		
		


}
