package oglasi.web.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import oglasi.model.Kategorija;
import oglasi.service.KategorijaService;
import oglasi.support.KategorijaToKategorijaDTO;
import oglasi.web.dto.KategorijaDTO;

@RestController
@RequestMapping(value = "/api/kategorije", produces = MediaType.APPLICATION_JSON_VALUE)
public class KategorijaController {
	
	@Autowired
	private KategorijaService kategorijaService;
	
	@Autowired
	private KategorijaToKategorijaDTO toKategorijaDTO;
	
	@GetMapping
	public ResponseEntity<List<KategorijaDTO>> getAll(){
		
		List<Kategorija> sve = kategorijaService.findAll();
		
		return new ResponseEntity<>(toKategorijaDTO.convert(sve),HttpStatus.OK);
	}
	
	
	@GetMapping(value="/{id}")
	public ResponseEntity<KategorijaDTO> getOne(@PathVariable Long id){
		
		Optional<Kategorija> kategorija = kategorijaService.findById(id);
		
		if(kategorija.isPresent()) {
			return new ResponseEntity<>(toKategorijaDTO.convert(kategorija.get()),HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
