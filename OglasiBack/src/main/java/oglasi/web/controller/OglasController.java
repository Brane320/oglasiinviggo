package oglasi.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import oglasi.model.Korisnik;
import oglasi.model.Oglas;
import oglasi.service.KorisnikService;
import oglasi.service.OglasService;
import oglasi.support.OglasDTOToOglas;
import oglasi.support.OglasToOglasDTO;
import oglasi.web.dto.OglasDTO;

@RestController
@RequestMapping(value = "/api/oglasi", produces = MediaType.APPLICATION_JSON_VALUE)

public class OglasController {
	
	@Autowired
	private OglasService oglasService;
	
	@Autowired
	private OglasToOglasDTO toOglasDTO;
	
	@Autowired
	private OglasDTOToOglas toOglas;

	@Autowired
	private KorisnikService korisnikService;
	
	@GetMapping									
	public ResponseEntity<List<OglasDTO>> getAll(
			@RequestParam(required=false) Long kategorijaId,
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) Double cenaOd,
			@RequestParam(required=false) Double cenaDo,
			@RequestParam(defaultValue="0") int brojStranice){
		
		
		Page <Oglas> svi = null;
		
		if(kategorijaId != null || naziv != null || cenaOd != null || cenaDo != null) {
			svi = oglasService.find(kategorijaId, naziv, cenaOd, cenaDo,brojStranice);
		} else {
			svi = oglasService.getAll(brojStranice);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.set("Total-Pages", svi.getTotalPages() + "");
		return new ResponseEntity<>(toOglasDTO.convert(svi.getContent()),headers,HttpStatus.OK);
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<OglasDTO> get(@PathVariable Long id){
		Optional<Oglas> oglas = oglasService.findById(id);
		
		if(oglas.isPresent()) {
			return new ResponseEntity<>(toOglasDTO.convert(oglas.get()), HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OglasDTO> create(@Valid @RequestBody OglasDTO oglasDTO){
	   Oglas oglas = toOglas.convert(oglasDTO);
       
	   
	   Oglas sacuvan = oglasService.save(oglas);
       
      

       return new ResponseEntity<>(toOglasDTO.convert(sacuvan), HttpStatus.CREATED);
    }
	
	@PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OglasDTO> update(@PathVariable Long id, @Valid @RequestBody OglasDTO oglasDTO){
		if(!id.equals(oglasDTO.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Oglas oglas = toOglas.convert(oglasDTO);
		
		Oglas izmenjen = oglasService.update(oglas);
		
		return new ResponseEntity<>(toOglasDTO.convert(izmenjen),HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<OglasDTO> delete(@PathVariable Long id){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String userName = auth.getName();
		
		Korisnik korisnik = korisnikService.findByKorisnickoIme(userName);
		
		Oglas oglas = oglasService.getOne(id);
		
		if(oglas.getKorisnik() == korisnik) {
		Oglas obrisan = oglasService.delete(id);
		
		
		if(obrisan != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		
	}
}
