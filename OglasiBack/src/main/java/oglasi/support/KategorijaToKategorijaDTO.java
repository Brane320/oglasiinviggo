package oglasi.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import oglasi.model.Kategorija;
import oglasi.web.dto.KategorijaDTO;

@Component
public class KategorijaToKategorijaDTO implements Converter<Kategorija,KategorijaDTO> {

	@Override
	public KategorijaDTO convert(Kategorija source) {
		KategorijaDTO dto = new KategorijaDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		
		return dto;
		
	}
	
	public List<KategorijaDTO> convert(List<Kategorija> kategorije){
		List<KategorijaDTO> dto = new ArrayList<>();
		
		for(Kategorija k : kategorije) {
			dto.add(convert(k));
		}
		return dto;
	}

}
