package oglasi.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import oglasi.model.Korisnik;
import oglasi.web.dto.KorisnikDTO;

@Component
public class KorisnikToKorisnikDTO implements Converter<Korisnik,KorisnikDTO>{

	@Override
	public KorisnikDTO convert(Korisnik source) {
		KorisnikDTO dto = new KorisnikDTO();
		
		dto.setBrojTelefona(source.getBrojTelefona());
		dto.setDatumRegistracije(source.getDatumRegistracije().toString());
		dto.setKorisnickoIme(source.getKorisnickoIme());
		
		return dto;
	}
	
	public List<KorisnikDTO> convert(List<Korisnik> korisnici){
		List<KorisnikDTO> dto = new ArrayList<>();
		
		for(Korisnik k : korisnici) {
			dto.add(convert(k));
		}
		
		return dto;
	}
}
