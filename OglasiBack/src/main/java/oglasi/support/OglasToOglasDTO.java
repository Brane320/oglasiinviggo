package oglasi.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import oglasi.model.Oglas;
import oglasi.web.dto.OglasDTO;

@Component
public class OglasToOglasDTO implements Converter<Oglas,OglasDTO>{

	@Override
	public OglasDTO convert(Oglas source) {
		OglasDTO dto = new OglasDTO();
		
		dto.setCena(source.getCena());
		dto.setDatumPostavljanja(source.getDatumPostavljanja().toString());
		dto.setGrad(source.getGrad());
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setKategorija(source.getKategorija());
		dto.setKorisnik(source.getKorisnik());
		dto.setOpis(source.getOpis());
		dto.setUrlSlike(source.getUrlSlike());
		
		return dto;
	}
	
	public List<OglasDTO> convert(List<Oglas> oglasi){
		List<OglasDTO> dto = new ArrayList<>();
		
		for(Oglas oglas : oglasi) {
			dto.add(convert(oglas));
		}
		return dto;
	}

}
