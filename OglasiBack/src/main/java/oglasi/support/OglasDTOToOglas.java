package oglasi.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import oglasi.model.Oglas;
import oglasi.service.OglasService;
import oglasi.web.dto.OglasDTO;

@Component
public class OglasDTOToOglas implements Converter<OglasDTO,Oglas> {
	
	@Autowired
	private OglasService oglasService;

	@Override
	public Oglas convert(OglasDTO dto) {
		
		Oglas oglas = null;
		
		if(dto.getId() != null) {
			oglas = oglasService.getOne(dto.getId());
		} else {
			oglas = new Oglas();
		}
		
		oglas.setCena(dto.getCena());
		oglas.setGrad(dto.getGrad());
		oglas.setId(dto.getId());
		oglas.setNaziv(dto.getNaziv());
		oglas.setKategorija(dto.getKategorija());
		oglas.setKorisnik(dto.getKorisnik());
		oglas.setUrlSlike(dto.getUrlSlike());
		oglas.setOpis(dto.getOpis());
		
		return oglas;
	}
	

}
