package oglasi.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import oglasi.model.Korisnik;
import oglasi.service.KorisnikService;
import oglasi.web.dto.KorisnikDTO;

@Component
public class KorisnikDTOToKorisnik implements Converter<KorisnikDTO,Korisnik>{
	
	@Autowired
	private KorisnikService korisnikService;

	@Override
	public Korisnik convert(KorisnikDTO dto) {
		Korisnik korisnik = null;
		
		if(dto.getKorisnickoIme() != null) {
			korisnik = korisnikService.findByKorisnickoIme(dto.getKorisnickoIme());
		}
		
		if(korisnik == null) {
			korisnik = new Korisnik();
			
		}
		
		korisnik.setBrojTelefona(dto.getBrojTelefona());
		korisnik.setKorisnickoIme(dto.getKorisnickoIme());
		korisnik.setDatumRegistracije(getLocalDate(dto.getDatumRegistracije()));
		
		return korisnik;
	}
	
	private LocalDate getLocalDate(String datumS) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate datum = LocalDate.parse(datumS, formatter);
     
        return datum;
    }

}
