Kako bi se aplikacija pokrenula potrebno je:
1. Importovati folder OglasiBack u Eclipse
2. Ući u paket Java Resources zatim u folder src/main/java pa u paket oglasi
3. Pokrenuti backend preko klase OglasiApplication.java (Run As > Java Application)
4. Otvoriti folder front-oglasi u Visual Studio programu
5. Otvoriti terminal i u terminalu upisati npm install
6. Kada se instalacija završi u terminalu upisati npm start,nakon toga će se i frontend aplikacije pokrenuti na localhost:3000

